import React from "react";
import {
    View,
    Text,
    StyleSheet,
    TouchableOpacity
} from "react-native";

const TodoList = ({ todos, toggleTodo }) => (
    <View style={styles.borderbox}>
        <Text style={{textAlign: 'center', padding: 10, fontSize: 18, textDecorationLine: 'underline', fontWeight: 'bold'}}> Task List </Text>
        {todos.map(todo =>
            <TouchableOpacity key={todo.id} onPress={() => toggleTodo(todo.id)}>
                <Text style={{
                    fontSize: 20,
                    textDecorationLine: todo.completed ? 'line-through' : 'none'
                }}>{todo.text}</Text>
            </TouchableOpacity>
        )}
    </View>
)
export default TodoList;

const styles = StyleSheet.create({
    borderbox: {
        marginTop: 20,
        marginLeft: 18,
        alignItems: 'center',
        justifyContent: 'center',
        width: "90%",
        padding: 20,
        borderWidth: 1,
        borderRadius:10,

    }
});