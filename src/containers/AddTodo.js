import React, { Component } from "react";
import {
    View,
    Text,
    StyleSheet,
    TextInput,
    TouchableOpacity
} from "react-native";
import { Ionicons } from '@expo/vector-icons'
import { connect } from 'react-redux'
import { addTodo } from '../actions'
class AddTodo extends Component {

    state={
        text:''
    }

    addTodo = (text) => {
        this.props.dispatch(addTodo(text))
        this.setState({ text: '' })
    }

    render() {
        return (
            <View style={styles.container}>
                <TextInput
                onChangeText={(text)=>this.setState({text})}
                value={this.state.text}
                style={styles.input}
                placeholder=" Create your task..."
                />
                <TouchableOpacity onPress={() => this.addTodo(this.state.text)}>
                    <View style={styles.addContent}>
                        <Ionicons
                            name="md-add" 
                            size={30}
                            style={styles.icon}
                        />
                    </View>
                </TouchableOpacity>
            </View>
        );
    }
}
export default connect()(AddTodo);

const styles = StyleSheet.create({ 
    container: {
        flexDirection: 'row',
        marginHorizontal: 20
    },

    input: {
        borderWidth: 1,
        borderColor: '#f2f2e1',
        backgroundColor: '#eaeaea',
        height: 50,
        flex: 1,
        padding: 5
    },

    addContent: {
        height: 50, 
        backgroundColor: '#eaeaea',
        alignItems: 'center',
        justifyContent: 'center'
    },

    icon: {
        color: '#de9595',
        padding: 10, 
    }

});